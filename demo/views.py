from django.shortcuts import render,redirect

import wizard_generate
import threading

# Create your views here.
def index(request):
    return render(request, 'demo/index.html')

# when generate button pressed
def generate(request):
    gen_thread = threading.Thread(target=wizard_generate.re_generate, args=(9,))
    gen_thread.start()
    gen_thread.join()
    # wizard_generate.re_generate(9)
    return redirect('index')