import os
import random
import math

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw 

import cv2
import numpy as np
import json
import glob

#path to fonts directory GLOBAL variable
file_cwd = os.path.dirname(os.path.realpath(__file__))
font_dir = os.path.join(file_cwd,"assets","fonts")
current_company = 'fantesy'

# images = glob.glob('assets/layouts/previews' + '/*.png')


class Color:
    """
    A class used for color representation (primarily rgb space)

    ...

    Attributes
    ----------
    r : int
        red channel of the color 
    b : int
        blue channel of the color
    g : int
        blue channel of the color 
    color : int
        representing all channels as a list
    flag : str
        used to determine whether the color should be made lighter or darker from its initial value
    factor: float
        amount by which the color must be made darker or lighter [0, 1)
    snew: float
        custom fractional value of saturation [0, 1]
    lnew: float
        custom fractional value of lightness [0, 1]

    Methods
    -------
    isLightOrDark()
        Decides whether the color is relatively light or dark using HSP equation
    lightdarkval()
        Finds the absolute lightness value from the HSP equation
    adjust_color_lightness(flag, factor)
        Adjusts colors lightness
    custom_sat_light(snew, lnew)
        Change color by customizing saturation and lightness
    """
    
    def __init__(self, color):
        """
        Parameters
        ----------
        color : int list
            color channels as a list in the RGB color space
        """
        
        self.r, self.g, self.b = color
        self.color = color
    
    def isLightOrDark(self):
        """
        Prints whether the color passed is light or dark from the HSP equation

        Returns
        ------
        string
            light or dark depending on the threshold
        """
        
        #HSP color model equation
        hsp = math.sqrt(0.299 * (self.r * self.r) + 0.587 * (self.g * self.g) + 0.114 * (self.b * self.b))
        if (hsp>127.5):
            return 'light'
        else:
            return 'dark'
        
    def lightdarkval(self):
        """
        Prints the value of the HSP equation with the passed color as input

        Returns
        ------
        float
            float value obtained from the HSP equation
        """
        
        return math.sqrt(0.299 * (self.r * self.r) + 0.587 * (self.g * self.g) + 0.114 * (self.b * self.b))
    
    def adjust_color_lightness(self, flag, factor = 0.1):
        """
        Converts the input color into its lighter or darker variant depending on the parameters passed

        Parameters
        ----------
        flag : str
            Value to indicte whether the color must be darj=ker or lighter than the input
        factor : float, optional
            The amount by which the color must be made darker or lighter (default is 0.1)

        Returns
        ------
        Color object
            Gives the converted color
        """
        
        h, l, s = rgb_to_hls(self.r / 255.0, self.g / 255.0, self.b / 255.0) 
        if flag == 'l':
            l = max(min(l * (1+factor), 1.0), 0.0)
        elif flag == 'd':
            l = max(min(l * (1-factor), 1.0), 0.0)
            
        r, g, b = hls_to_rgb(h, l, s)
        return Color((int(r * 255), int(g * 255), int(b * 255)))
    
    def custom_sat_light(self, snew, lnew):
        """
        Converts the input color into another color based on custom saturation and lightness values

        Parameters
        ----------
        snew : float
            The custom fractional value of saturation 
        lnew : float
            The custom fractional value of lightness

        Returns
        ------
        Color object
            Gives the converted color
        """
        
        h, l, s = rgb_to_hls(self.r / 255.0, self.g / 255.0, self.b / 255.0)
        l, s = lnew, snew
        r, g, b = hls_to_rgb(h, l, s)
        return Color((int(r * 255), int(g * 255), int(b * 255)))   

    

def banner_bg_col(theme, palette_dict):
    """
    choose a random palette and from that two colors

    Parameters
    ----------

    Returns
    ------
    
    """
    
    if theme in palette_dict:
        col1, col2 = random.sample(palette_dict[theme], 2)
        #lightest color becomes the background color, darkest becomes the banner color
        if col1.lightdarkval()>col2.lightdarkval():
            bgcol, bannercol = col1, col2
        else:
            bgcol, bannercol = col2, col1
    
    return bgcol, bannercol

# code to generate posters randomly

def get_company_logo():
    """
    This will return the logo of the company for which the poster generation is happening
    
    Returns
    ------
    RGBA logo PIL image object
    """
    logo = Image.open(os.path.join(file_cwd,'assets','logos',current_company+'.png')).convert("RGBA")
    return logo


def find_font(fontname):
    """
    function to detect most appropriate font style file from the library
    
    Parameters
    ----------
    fontname : Name of the font extracted from layout

    Returns
    ------
    font file path
    """
    default_font = "roboto-bold.ttf"
    result = []
    style_req = fontname.lower().replace("-"," ").replace("_"," ")  #make the fontname lowercase and eliminate -,_ characters
    family = style_req.split()[0]  #font family name from the given font style
    font_files = os.listdir(font_dir)  #list of all the fonts available
    for file in font_files:
        style = file.lower().replace("-"," ").replace("_"," ") 
        if (style.find(family) == -1):  #no match
            continue
        else:                          #stores all the fonts belonging to the family
            result.append(file)
    if (result == []):  #no font found
            return os.path.join(font_dir,default_font) 
    for font in result:   #check for perfect match
        if style_req +".ttf"==font.lower().replace("-"," ").replace("_"," "):
            return os.path.join(font_dir,font)
    else:   #return a font from the same family
        result.sort()
        return os.path.join(font_dir, result[0])

#function to find fontsize
def get_font_size(text, font_path, height, width):
    """
    function to find fontsize
    Parameters
    ----------

    Returns
    ------
    
    """
    #starting from 1, the fontsize gets adjusted comparing the sizes of box and font
    fontsize = 1
    jumpsize = 60
    font = ImageFont.truetype(font_path,fontsize)
    while True:
        if (font.font.getsize(text)[0][0] < width and (font.getmetrics()[0] -font.font.getsize(text)[1][1])<height):
            fontsize += jumpsize
        else:
            jumpsize = jumpsize // 2
            fontsize -= jumpsize
        font = ImageFont.truetype(font_path, fontsize)
        if jumpsize <= 1:
            break
    return fontsize


def paste_img_box(img_draw,box, bgcol, bannercol):
    """
    function to paste content into box

    Parameters
    ----------

    Returns
    ------
    
    """
    # box type is image. select the aproprite class using tags and type and paste it
    if box['box_type'] in {'image','product_image','company_logo','text_object','shape_colored','background'}:

        #select image using tag (best choise or random choise)
        if (box['box_type']=='product_image'):
            img_dir=os.path.join(file_cwd,'assets','product_images')
            imgs=[os.path.join(img_dir,i) for i in os.listdir(img_dir)]
            image_file = Image.open(imgs[int(random.random()*100)%len(imgs)]).convert("RGBA")
        
        elif (box['box_type']=='company_logo'):
            image_file = get_company_logo() #get current working company logo image
        
        elif (box['box_type']=='text_object'):
            img_dir=os.path.join(file_cwd,'assets','text_objects')
            imgs=[os.path.join(img_dir,i) for i in os.listdir(img_dir)]
            image_file = Image.open(imgs[int(random.random()*100)%len(imgs)]).convert("RGBA")
            
        
        elif (box['box_type']=='shape_colored'):
            img_dir=os.path.join(file_cwd,'assets','shapes_colored','2.jpg')
            banner = cv2.imread(img_dir)

            #creating an image of a solid color
            background_banner = np.zeros(banner.shape, np.uint8)
            background_banner[:] = (bannercol.b, bannercol.g, bannercol.r)

            #blending the texture and the solid color
            banner_final = cv2.addWeighted(banner, 0.5, background_banner, 0.5, 0.0)
            image_file = Image.fromarray(cv2.cvtColor(banner_final, cv2.COLOR_BGR2RGB))
            image_file = image_file.convert("RGBA")
        
        elif (box['box_type']=='background'):
            img_dir=os.path.join(file_cwd,'assets','background_overlays')
            imgs=[os.path.join(img_dir,i) for i in os.listdir(img_dir)]
            bg_texture = cv2.imread(imgs[int(random.random()*100)%len(imgs)])
            #creating an image of a solid color
            bg_img = np.zeros(bg_texture.shape, np.uint8)
            bg_img[:] = (bgcol.b, bgcol.g, bgcol.r)

            #blending the texture and the solid color
            bg_final = cv2.addWeighted(bg_texture, 0.5, bg_img, 0.5, 0.0)
            image_file = Image.fromarray(cv2.cvtColor(bg_final, cv2.COLOR_BGR2RGB)).convert("RGBA")
            
        else:
            display(box['box_type'],'returning')
            return img_draw


        #resizing image according to the box
        box_hw_ratio = float(box['box_h'])/float(box['box_w'])
        image_hw_ratio = float(image_file.size[1])/float(image_file.size[0])
        
        x_offset = y_offset = 0
        if (box_hw_ratio>=image_hw_ratio):# fix using box width
            new_image_height = int(image_hw_ratio * box['box_w'])
            new_image_width = box['box_w']
            y_offset = int((box['box_h'] - new_image_height)/2.0)
        else:
            new_image_height = box['box_h']
            new_image_width = int(box['box_h'] / image_hw_ratio)
            x_offset = int((box['box_w'] - new_image_width)/2.0)

        image_file = image_file.resize((new_image_width,new_image_height))
            
        img_draw.paste(image_file,(box["box_x"]+x_offset,box["box_y"]+y_offset),image_file)#offset
        return img_draw
    # box type is text. Generate and paste using font files
    elif box['box_type'] == 'text':                             # reference link: https://www.blog.pythonlibrary.org/2021/02/02/drawing-text-on-images-with-pillow-and-python/
        txt = box["type_specific"]["text"].replace("\r","\n")   #text content
        font_style = box["type_specific"]["font"]               #font style
        font_path = find_font(font_style.lower())               #font file from directory
        alignment = box["type_specific"]["alignment"]           #alignment (center,left,right)
        fontsize = get_font_size(txt,font_path,box["box_h"],box["box_w"]) #font size based on height and width of bounding box
        font = ImageFont.truetype(font_path,fontsize)           #font with style and size
        d = ImageDraw.Draw(img_draw)              
        d.multiline_text((box["box_x"],box["box_y"]),txt,font=font,align = alignment,fill=(0,0,0))
        return img_draw
    
    # other types smart objects overlays ..etc TODO
    else:
        return img_draw

def re_generate(count=6):
    # print('hi')
    palette_dict = {}

    #adding custom palettes
    palette_dict['pool'] = [[19,105,149],[231,240,244],[113,165,191],[127,174,197],[137,190,194],[222,240,237],[0,152,241],[150, 186, 255],[136, 255, 247],[162, 219, 250]]
    palette_dict['pink'] = [[231,84,105],[227,116,123],[236,147,167],[250,205,215],[250,230,225],[253,230,235],[253,249,250],[255,255,255],[255,183,197],[252,142,172]]
    palette_dict['purple'] = [[100,64,90],[158,112,137],[173,150,169],[206,189,182],[233,157,156]]
    palette_dict['pear'] = [[134,160,150],[76,81,82],[218,219,185],[128,112,106],[130,131,111],[192,193,183]]

    #converting the dict elements into colour class objects
    palette_dict['pool'] = [Color(color) for color in palette_dict['pool']]
    palette_dict['pink'] = [Color(color) for color in palette_dict['pink']]
    palette_dict['purple'] = [Color(color) for color in palette_dict['purple']]
    palette_dict['pear'] = [Color(color) for color in palette_dict['pear']]

    # opening a single layout
    sample_layout = json.load(open(os.path.join(file_cwd,'assets','layouts','fantasy01.psd.json')))


    # current_company = 'jouda-fantasy' # 'fantasy', 'cutelady-fantasy','nidha-fantasy', 'alhandam-fantasy', 'jouda-fantasy','estern-fantasy'
    # theme = 'pink' #'pool' 'purple'

    posters_per_brand = count
    # generating 'n' random images for each comapany
    global current_company
    for current_company in ['fantasy', 'cutelady-fantasy','nidha-fantasy', 'alhandam-fantasy', 'jouda-fantasy','eastern-fantasy']:
        theme = ['pink','pool','purple','pear'][int(random.random()*100)%4]
        try:
            os.mkdir(os.path.join(file_cwd,'demo','static','out',current_company))
        except FileExistsError:
            t_do_nothing = True
        for k in range (posters_per_brand):
            background_img = Image.new(mode = "RGB", size = (sample_layout['height'],sample_layout['width']), color = (255,255,255))
            bgcol, bannercol = banner_bg_col(theme, palette_dict) 
            # bgcol, bannercol = banner_bg_col(random.choice(list(palette_dict.keys())), palette_dict) 

            for i in range (len(sample_layout['boxes'])):
                box = sample_layout['boxes'][-(i+1)] # we need to pase them in the reverse order
                background_img = paste_img_box(background_img,box, bgcol, bannercol)

            background_img.save(os.path.join(file_cwd,'demo','static','out',current_company,str(k)+'.png'))
            # display(background_img)

        